using System;
using System.Collections.Generic;

namespace MIST
{
    [Serializable()]
    public class TypingResultsCollection
    {
        private List<TypingResults> _results = new List<TypingResults>();

        private InSilicoTyping _analysis;
        private bool _showFuzzyMatching;

        private Dictionary<string, TestType> _testTestTypesDict = new Dictionary<string, TestType>();
        private Dictionary<string, HashSet<Marker>> _testMarkerDict = new Dictionary<string, HashSet<Marker>>();
        private Dictionary<string, ExtraTestInfo> _testExtraInfoDict = new Dictionary<string, ExtraTestInfo>();
        private Dictionary<string, HashSet<MarkerMatch>> _testMatchesDict = new Dictionary<string, HashSet<MarkerMatch>>();

        public List<TypingResults> Results { get { return _results; } set {  } }
        public Dictionary<string, TestType> TestTypes { get { return _testTestTypesDict; } set { } }
        public Dictionary<string, ExtraTestInfo> TestMetadataFile { get { return _testExtraInfoDict; } set { } }
        public Dictionary<string, HashSet<Marker>> TestMarkerDict { get { return _testMarkerDict; } }

        public Dictionary<string, List<Marker>> TestMarkers
        {
            get
            {
                var rtn = new Dictionary<string, List<Marker>>();
                foreach (var pair in _testMarkerDict)
                {
                    string testName = pair.Key;
                    var markers = new List<Marker>(pair.Value);
                    rtn.Add(testName, markers);
                }
                return rtn;
            }
            set {}
        } 

        public TypingResultsCollection(InSilicoTyping analysis, bool showFuzzyMatching)
        {
            _analysis = analysis;
            _showFuzzyMatching = showFuzzyMatching;
            GetTestInformation();
            GetResults();
        }

        private void GetTestInformation()
        {

            foreach (ContigCollection multiFastaFile in _analysis.MultiFastaFiles)
            {
                foreach (MarkerMatch markerMatch in multiFastaFile.MarkerMatches)
                {
                    var testName = markerMatch.TestName;
                    if (_testTestTypesDict.ContainsKey(testName))
                    {
                        _testMarkerDict[testName].Add(markerMatch.Marker);
                        _testMatchesDict[testName].Add(markerMatch);
                    }
                    else
                    {
                        _testTestTypesDict.Add(testName, markerMatch.Marker.TypingTest);
                        _testMarkerDict.Add(testName, new HashSet<Marker> { markerMatch.Marker });
                        _testMatchesDict.Add(testName, new HashSet<MarkerMatch> { markerMatch });

                        ExtraTestInfo extra = null;
                        foreach (ExtraTestInfo extraInfo in _analysis.ExtraInfo)
                        {
                            if (extraInfo.TestName != testName)
                                continue;
                            extra = extraInfo;
                            break;
                        }
                        _testExtraInfoDict.Add(testName, extra);
                    }
                }
            }
        }

        private void GetResults()
        {
            foreach (ContigCollection contigCollection in _analysis.MultiFastaFiles)
            {
                _results.Add(new TypingResults(contigCollection, this, _showFuzzyMatching));
            }
        }
    }
}
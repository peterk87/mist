﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace MIST
{
    //Need the ability to detect and adjust analysis for degenerate primers
    [Serializable()]
    public class Marker : IComparable<Marker>
    {
        private string _allelicDatabaseFilename;
        private double _ampliconRange = -1;
        private int _ampliconSize;
        private string _forwardPrimer;
        private string _name;
        private int _repeatSize;
        private string _reversePrimer;
        private string _testName;
        private TestType _typingTest;

        public Marker(
            string name,
            string testName,
            TestType testType,
            string fprimer,
            string rprimer,
            int ampliconSize,
            double ampliconRange,
            string allelicDatabaseFilename,
            int repeatSize
            )
        {
            _typingTest = testType;
            _name = name;
            _testName = testName;
            _forwardPrimer = fprimer.ToUpper();
            _reversePrimer = rprimer.ToUpper();
            _ampliconSize = ampliconSize;
            _ampliconRange = ampliconRange;
            _allelicDatabaseFilename = allelicDatabaseFilename;
            _repeatSize = repeatSize;
            Selected = true;
        }

        public bool Selected { get; set; }

        public string TestName
        {
            get { return _testName; }
            set { _testName = value; }
        }

        public TestType TypingTest
        {
            get { return _typingTest; }
            set { _typingTest = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string ForwardPrimer
        {
            get { return _forwardPrimer; }
            set { _forwardPrimer = value; }
        }

        public string ReversePrimer
        {
            get { return _reversePrimer; }
            set { _reversePrimer = value; }
        }

        public int AmpliconSize
        {
            get { return _ampliconSize; }
            set { _ampliconSize = value; }
        }
        /// <summary>Amplicon range factor for PCR assays. BLAST %ID threshold for probe tests.</summary>
        public double AmpliconRange
        {
            get { return _ampliconRange; }
            set { _ampliconRange = value; }
        }

        public int UpperAmpliconRange
        {
            get { return (int)Math.Ceiling(_ampliconSize * (1d + _ampliconRange)); }
        }

        public int LowerAmpliconRange
        {
            get { return (int)Math.Floor(_ampliconSize * (1d - _ampliconRange)); }
        }

        public string AllelicDatabaseFilename
        {
            get { return _allelicDatabaseFilename; }
            set { _allelicDatabaseFilename = value; }
        }

        public int RepeatSize
        {
            get { return _repeatSize; }
            set { _repeatSize = value; }
        }

        public List<Contig> Alleles
        {
            get
            {
                if (_typingTest != TestType.Allelic)
                    return null;
                if (!Program.OutputAlleleSequences)
                    return null;
                var alleles = new ContigCollection(_allelicDatabaseFilename);
                alleles.Read();
                return alleles.Contigs;
            }
            set {}
        } 

        #region IComparable<marker> Members

        public int CompareTo(Marker other)
        {
            return String.CompareOrdinal(_name, other.Name);
        }

        #endregion

        public override string ToString()
        {
            var tmp = new List<string>
                          {
                              _name,
                              _testName,
                              ((int) _typingTest).ToString(CultureInfo.InvariantCulture),
                              _forwardPrimer,
                              _reversePrimer,
                              _ampliconSize.ToString(CultureInfo.InvariantCulture),
                              _ampliconRange.ToString(CultureInfo.InvariantCulture),
                              _allelicDatabaseFilename,
                              RepeatSize.ToString(CultureInfo.InvariantCulture)
                          };

            return string.Join("\t", tmp);
        }
    }
}
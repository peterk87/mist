﻿using System;

namespace MIST
{
    /// <summary>Fasta entry object. Contains nucleotide sequence, header string, index in multifasta and origin multifasta object.</summary>
    [Serializable()]
    public class Contig
    {
        /// <summary>Header string.</summary>
        private string _header;

        /// <summary>Fasta entry index in multifasta file (starting from 0)</summary>
        private int _index;

        /// <summary>Origin multifasta file.</summary>
        private ContigCollection _multifastaFile;

        /// <summary>Nucleotide sequence</summary>
        private string _sequence;

        /// <summary>Constructor for fasta entry/Contig object.</summary>
        /// <param name="h">Header string.</param>
        /// <param name="index">Index of fasta entry in multifasta file.</param>
        /// <param name="seq">Nucleotide sequence of fasta entry</param>
        /// <param name="multifastaFile">Origin multifasta file</param>
        public Contig(string h, int index, string seq, ContigCollection multifastaFile)
        {
            _header = h;
            _index = index;
            _sequence = seq;
            _multifastaFile = multifastaFile;
        }

        /// <summary>Origin multifasta file.</summary>
        public ContigCollection MultifastaFile { get { return _multifastaFile; }  }

        /// <summary>Header string.</summary>
        public string Header { get { return _header; } set { _header = value; } }

        /// <summary>Nucleotide sequence</summary>
        public string Sequence { get { return _sequence; } set { _sequence = value; } }

        /// <summary>Fasta entry index in multifasta file (starting from 0)</summary>
        public int Index { get { return _index; } set { _index = value; } }

        /// <summary>Set sequence to null. Free up some memory.</summary>
        public void ClearSequence()
        {
            _sequence = null;
        }
    }
}